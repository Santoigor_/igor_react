import React from 'react';
import { slide as Menu } from 'react-burger-menu';
import '../assets/css/menu.css';
import  {BrowserRouter as Link} from 'react-router-dom';


const Example = () => {
            const showSettings = (event) => {
                event.preventDefault();
            }

                return (
                <Menu>
                    <Link to="/" id="home" className="menu-item">Home</Link>
                    <Link to="/about" id= "about" className="menu-item">About</Link>
                    <Link to ="/contact" id="contact" className="menu-item">Contact </Link>
                    <Link onClick={ showSettings } className="menu-item--small">Settings </Link>
                </Menu>
                );
            }
        

            export default Example;