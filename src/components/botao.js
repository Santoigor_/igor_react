import React from 'react';
import '../assets/css/botao.css';
const Botao = (props) => {
    return(
        <button Onclick={props.btnFunction}>{props.children}</button>
    );
}

export default Botao;