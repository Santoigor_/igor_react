import Calendar from 'react-calendar';

const Calendario = () => {
    state = {
        date = new Date()
    }

    onChange = date => this.setState({date})

    return (
        <Calendar
        onChange = {this.onChange}
        value = {this.state.date} />
    );
}

export default Calendario;
