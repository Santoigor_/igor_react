import React from 'react';
import {ReactComponent as Ping} from './Ping.svg';
import {ReactComponent as Img1} from './Frame.svg';
import {ReactComponent as Img2} from './Frame 1.svg';
import {ReactComponent as Img3} from './Frame 2.svg';
import {ReactComponent as Img4} from './Ping de 4.svg';
import '../assets/css/reset.css'
import Example from '../components/menu'
import '../assets/css/pigbank.css'
import Botao from '../components/botao';

const Home = () => {
    return (
        <React.Fragment>
            <Example />
            <section className = "sec1">
                <div className = "sec_content">
                    <div className="left">
                        <h1>Ping Bank</h1>
                        <div className="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a leo eu enim auctor faucibus id eget dolor. Pellentesque faucibus leo sed arcu vulputate rhoncus.</div>
                        <Botao Onclick={() => {}}>Nova Meta</Botao>
                    </div>
                    <Ping  className="Ping"/>
                </div>
            </section>
            <section className="sec2">
                <p>Como usar</p>
                <div className="sec2_content">
                    <div className="card">
                        <Img1/>
                        <p className="title">Defina uma meta</p>
                        <p className="text">Pellentesque faucibus leo sed arcu vulputate rhoncus. Integer sit amet blandit felis</p>
                    </div>
                    <div className="card">
                        <Img2/>
                        <p className="title">Acompanhe o progresso</p>
                        <p className="text">Pellentesque faucibus leo sed arcu vulputate rhoncus. Integer sit amet blandit felis</p>
                    </div>
                    <div className="card">
                        <Img3/>
                        <p className="title">Alcance seu objetivo</p>
                        <p className="text">Pellentesque faucibus leo sed arcu vulputate rhoncus. Integer sit amet blandit felis</p>
                    </div>
                </div>
            </section>
            <section className="sec3">
                <div className="sec3_content">
                    <div className="left">
                        <p className="title">Já tem uma meta ?</p>
                        <p className="text">Phasellus mollis eget enim sed malesuada. Nulla facilisi. Aliquam nunc lacus, commodo hendrerit commodo pulvinar, suscipit eget ipsum. Nulla at sem ex.</p>
                        <Botao>Ver Meta</Botao>
                    </div>
                    <Img4 />
                </div>
            </section> 
            <section className="sec4">
                <p>Não tem? Comece agora mesmo</p>
                <Botao>Nova Meta</Botao>
            </section>
        
        </React.Fragment>
    );
};

export default Home;