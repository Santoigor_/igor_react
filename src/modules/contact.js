
import React from 'react';
import { Formik } from 'formik';
import API from './api';
import '../assets/css/contact.css';
import Calendario from '../modules/contact'

const Basic = () => {
    API.get('/categories').then(console.log);

return (
    <Formik initialValues={{ title: '', quantia: ''}}
     
      onSubmit={(values,{setSubmitting}) => {

          console.log('enviado');
          setSubmitting(false);
       }}
    >
      {({
        values,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
      }) => (
        <div className="form">
          <form className="column" onSubmit={handleSubmit}>

            <div className="content_form">
              <label>Titulo</label>
            
              <input className="input"
                  type="text"
                  name="title"
                  placeholder="Nome da meta"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.title}
                />
            </div>

            <div className="container">
              <label>Quantia:</label>
              <input className="input"
                type="text"
                name="quantia"
                placeholder="Quantia em $ a economizar "
                onChange={handleChange}
                value={values.quantia}
              />
            </div>

            
            <div className="container">
              <label>Categoria:</label>
              <div className="display spaceBetween">
                  <input className="input"
                  type="text"
                  name="categoria"
                  placeholder="Nome da categoria"
                  onBlur={handleBlur}
                  value={values.categoria}
                  />
                  <input className="inputMenor"
                  type="text"
                  name="color"
                  placeholder="Cor desejada (#)"
                  onBlur={handleBlur}
                  value={values.color}
                  />
              </div>
            </div>

            <div className="container">
              <label>Descrição:</label>
              <input className="inputArea"
                type="textarea"
                name="descricao"
                onChange={handleChange}
                value={values.descricao}
              />
            </div>
            </form>
            <Calendario className="calendario"/>
            <button  onClick = "API.post(/goals, values)"  type="submit" disabled={isSubmitting}>
              Submit
            </button>

        </div> 
      )}
    </Formik>
    );
}
export default Basic;

