import axios from "axios";

 const API = axios.create({
    baseURL: "http://in-pig-bank.herokuapp.com/",
    respondeType: "json"
});

export default API;