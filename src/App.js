import React from 'react';
// import logo from './logo.svg';
import './App.css';
import Home from './modules/home';
import Basic from './modules/contact';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
const App = () => {
  return (
<Router>
    <Switch>
      <Route exact path='/'><Home/></Route>
      <Route exact path='/novameta'><Basic/></Route>
      </Switch>
   </Router>
  );
}

export default App;
